package joshbaryia.GraphOps

import org.apache.spark.graphx._
import org.apache.spark.graphx.lib.ShortestPaths
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._

/**
  * Created by joshbaryia on 09/04/2016.
  *
  * Class contains various utilities for running centrality measures
  *
  * In each case, centrality is
  *
  */
object GraphCentrality {


  /**
    * Computes the focused centrality of a graph given a list of keywords.
    * Returns an RDD of vertices and their centrality scores
    * */
  def focusedCloseness(graph: Graph[String, String], sources: List[String]): VertexRDD[Double] = {

    // Produce a list of source vertexIDs
    val sourceVertices =
      graph.subgraph(
        vpred = (id, vertexName) => {
          var isSource = false
          for (k <- sources) {
            if (vertexName.toLowerCase().contains(k.toLowerCase())) isSource = true
          }
          isSource
        }
      )
        .vertices
        .map(v => v._1)
        .collect.toList

    println("Number of landmarks: " + sourceVertices.length)

    /**
      * For each vertex, create a map of shortest paths from the vertex in question to each source vertex.
      * Then reduce these results into one final focused centrality score
      * */
    ShortestPaths.run(graph, sourceVertices)
        .mapVertices((id, spMap) => {
          spMap
            .map(v => v._2)
            .reduce(_ + _).asInstanceOf[Double]
        })
      .vertices
  }

  /**
    *
    *  NB Doesnt scale.  Vertices need to hold a map of all other source vertices, n**2 memory use
    *
    * */
  def closeness(graph: Graph[String, String]): VertexRDD[Double] = {
    ShortestPaths.run(graph, graph.vertices.map(v => v._1).collect().toList)
      .mapVertices((id, map) => {
        map.map(v => v._2)
          .reduce(_ + _).asInstanceOf[Double]
      })
      .vertices
  }

}
