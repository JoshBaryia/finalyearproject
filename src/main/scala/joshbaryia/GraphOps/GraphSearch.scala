package joshbaryia.GraphOps

import org.apache.spark.graphx.{EdgeDirection, GraphOps, VertexRDD, Graph}
import org.apache.spark.SparkContext._
import org.apache.spark.SparkContext

/**
  * Created by joshbaryia on 09/04/2016.
  */
object GraphSearch {


  def spreadActivationWithKeywords(graph: Graph[String, String],
                                   keywords: List[String],
                                   firetol: Double = 1d,
                                   maxSteps: Int = 3, // amount of supersteps in each pregel run
                                   maxWaves: Long = 2): VertexRDD[Double] = {

    case class SAVert(name: String, activation: Double, activated: Boolean, shouldFire: Boolean = false, degree: Int = -1)

    val initialMsg = 0d
    /**
      * Initialise the graph.  All vertices containing keywords are initialised to be source vertices.
      * Also attach each vertex's degree
      * */
    var g = graph.mapVertices((id, vertexName) => {
      var isSource = false
      for (keyword <- keywords) {
        if (vertexName.toLowerCase.contains(keyword.toLowerCase)) {
          isSource = true
        }
      }
      SAVert(vertexName,
        activation = if (isSource) 1 else 0d,
        activated = isSource,
        shouldFire = isSource)

    })
      .outerJoinVertices(graph.degrees)((id, v, deg) => SAVert(v.name, v.activation, v.activated, degree=deg.get))
    var wave: Int = 0

    do {
      g = g
        /**
          * Beginning of a wave.  Initialise all vertices that were previously active to fire again
          * Otherwise verte gets initialised to zero
          * */
        .mapVertices((id, v) => {
          if (v.activated || v.activation >= firetol) {
            SAVert(v.name, v.activation, true, shouldFire = true)
          }else {
            SAVert(v.name, v.activation, false) // ?? activation -> v.activation?
          }
        })
        .pregel(initialMsg, maxSteps, EdgeDirection.Either)(
          /**
            * If a vertex's value goes over the tolerance value, it should fire the next round
            * If it has already fired it should not fire again
            * It should accumulate activation each superstep
            * */
          vprog = (id, v, msg) => {
            val newActivationValue = v.activation + msg
            val active = (newActivationValue >= firetol)
            // only fire if werent previously active but are now
            val fireNextStep =  (newActivationValue >= firetol /*&& v.activation < firetol && !v.shouldFire*/)

            SAVert(name = v.name, activation = newActivationValue, activated = active, shouldFire = fireNextStep, degree = v.degree)
          },
          mergeMsg = (m1, m2) => m1 + m2,
          sendMsg = (triplet) => {
            if (triplet.srcAttr.shouldFire) {
              val toSend = triplet.srcAttr.activation
              Iterator((triplet.dstId, toSend))
            }
            if (triplet.dstAttr.shouldFire) {
              val toSend = triplet.dstAttr.activation
              Iterator((triplet.srcId, toSend))
            }
            else {
              Iterator.empty
            }
          }
      )

      val totalActivation = g.vertices.map(vert => vert._2.activation).reduce(_ + _)
      val totalActivatedVerts = g.vertices.filter(_._2.activated).count


      //println("Wave: " + wave + " total: " + totalActivation + " activated " + totalActivatedVerts)

      wave = wave + 1
    } while (wave < maxWaves)

    /**
      * Return as RDD of vertices activated by the spreading activation algorithm
      * */
    println("Returning from graphsearch")
    g
      .subgraph(
        vpred = (id, vval) =>  vval.activation > 0
      )
      .mapVertices((id, vert) => vert.activation).vertices

  }

  def breadthFirstSearch(graph: Graph[String, String], sources: List[String],
                         maxSteps: Int = 3
                        ): VertexRDD[Double] = {

    case class BFSVertex(name: String, value: Double)

    println("BFS: keys: " + sources.map(s => s + " ").reduce(_ + _))

    /**
      * Initialise the source vertices to one and all other vertices to zero
      * */
    graph.mapVertices((id, name) => {
      var isSource = false
      for (k <- sources) {
        if (name.toLowerCase().contains(k.toLowerCase())) {
          isSource = true
        }
      }
      if (isSource)
        1d else 0d
    })
      .pregel (0d, maxSteps, EdgeDirection.Either)(
        vprog = (id, v, m) =>  v + m,
        mergeMsg = (m1, m2) => m1 + m2,
        sendMsg = triplet => {
          Iterator((triplet.dstId, triplet.srcAttr))
        }
      )
      /**
        * Filter out all untouched vertices
        * */
      .subgraph(
        vpred = (id, vval) => vval > 0
      )
      .vertices

  }

}
