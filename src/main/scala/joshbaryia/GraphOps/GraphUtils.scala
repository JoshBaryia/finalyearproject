package joshbaryia.GraphOps

import akka.routing.MurmurHash
import joshbaryia.Driver.VertexValue
import org.apache.spark.graphx.{Edge, VertexRDD, VertexId, Graph}
import org.apache.spark.SparkContext._
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

import scala.reflect.ClassTag

/**
  * Created by joshbaryia on 11/04/2016.
  */
object GraphUtils {

  /**
    * Takes a graph and returns an RDD of all vertex IDs within the largest connected component
    *
    * */
  def largestConnectedComponent[VD: ClassTag, ED: ClassTag](graph: Graph[VD, ED]): VertexRDD[Double] = {

    /**
      * Get largest component ID
      * */

    val ccGraph = graph.connectedComponents().cache()
    println("largest cc size: " + ccGraph.vertices.count())


    val largestComponentId = ccGraph
      .vertices
      .map(vertex => (vertex._2, 1))
      .reduceByKey(_ + _)
      .sortBy(_._2, ascending=false)
      // Take the first element (the id of the largest component)
      .take(1)(0)._1

    println("CCID: " + largestComponentId)

    /**
      * Now filter out all components without the max ID
      * */
    ccGraph
      .subgraph(
        vpred = (id, vval) => vval == largestComponentId
      )
      .mapVertices((id, vertval) => 1d)
      .vertices

  }


  def graphLoader[VD: ClassTag, ED: ClassTag](textFile: RDD[String]): Graph[VertexValue, String] = {



    val verts: RDD[(VertexId, VertexValue)] = textFile
      /**
        * Removes all literals
        * */
      .filter(line => !line.contains("\"")) // @ ^
      .map(
      line => {
        val vtext = line.trim().split(" ")(0)
        (MurmurHash.stringHash(vtext).asInstanceOf[VertexId], VertexValue(vtext, 0, 0))
      })
      .union(textFile.map(
        line => {
          val vtext = line.trim().split(" ")(2)
          (MurmurHash.stringHash(vtext).asInstanceOf[VertexId], VertexValue(vtext, 0, 0))
        })
      ).distinct()

    println("lines: " + textFile.count())

    val edges: RDD[Edge[String]] = textFile.map(
      line => {
        val tokes = line.trim().split(" ")
        val srcId = MurmurHash.stringHash(tokes(0))
        val edgeAttr = tokes(1)
        val dstId = MurmurHash.stringHash(tokes(2))

        Edge(srcId, dstId, edgeAttr)
      }
    )

    val graph = Graph(verts, edges)

    graph
      .outerJoinVertices(graph.degrees)((id, vval, deg) => VertexValue(vval.name, 0, deg.get))

  }


  /**
    * Removes undesirable vertices, such as stopverts.
    *
    * Returns IDS of vertices in the largest connected component of the new subgraph
    * */
  def removeStopVertices[VD: ClassTag, ED: ClassTag](graph: Graph[VD, ED], vertexFilterTolerance: Double = 300000): VertexRDD[Double] = {

    graph
      /**
        * Since sink vertices are decided on by their structure no vertex data is needed.
        * A high outdegree shows a vertex is well described
        * A high indegree likely represents an abstract concept
        * */
      .mapVertices((id, vval) => 1)
      .outerJoinVertices(graph.inDegrees)(
        (id, vertval, indegree) => indegree.getOrElse(1))
      .outerJoinVertices(graph.outDegrees)(
        (id, vertval, outdegree) => vertval/outdegree.getOrElse(1))
      .outerJoinVertices(graph.degrees)(
        (id, vertval, totaldegree) => vertval*totaldegree.getOrElse(1))
      .mapVertices((id, vert) => vert.asInstanceOf[Double])
      /**
        * Penalise vertices with high value as these likely represent sink vertices
        * */
      .subgraph(
        vpred = (id, vertval) => vertval < vertexFilterTolerance
      )
      .vertices
  }


}
