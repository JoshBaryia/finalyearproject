package joshbaryia

import org.apache.log4j.{Level, LogManager, Logger}
import org.apache.spark.SparkContext._
import org.apache.spark.SparkContext
import akka.routing.MurmurHash
import org.apache.spark.SparkConf
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD

import joshbaryia.GraphOps._
/**
  *  Josh Baryia
  * */

object Driver {

  case class VertexValue(name: String, vval: Double, degree: Int, shouldFire: Boolean = false, activation: Float = 0, inoutratio: Float = -1, activated: Boolean = false, sourceVert: Boolean = false)

  val conf: SparkConf = new SparkConf()
    .setAppName("Semstim test")
    .setMaster("local[*]")
    .set("spark.driver.memory", "4g")
    .set("spark.executor.memory", "4g")

  val sc: SparkContext = new SparkContext(conf)
  val LOG = Logger.getLogger(getClass.getName)

  var inputpath = ""
  var outputpath = ""
  var keywords: List[String]  = List()

  def main(args: Array[String]) {

    LogManager.getRootLogger.setLevel(Level.WARN)
    LOG.info("Driver started")

    inputpath = args(0)
    outputpath = args(1)

    /**
      * Some input validation etc.
      * */
    if (args.length < 1 ) {
      LOG.error("[ERROR] Please use the following format: inputpath outputsavepath (- to not persist) .. keywords")
      System.exit(1)
    }
    if ("-".equals(outputpath.trim)) {
      println("Not persisting results")
    } else {
      println("Persisting results to: " + outputpath)
    }

    val keys = List("jimi_hendrix", "bob_dylan", "louis_armstrong","the_beatles", "david_bowie", "stevie_ray_vaughan", "rolling_stones")

    keywords = args.slice(2, args.length).toList
    LOG.info("Keywords: " + keywords.reduce(_ + " " + _))

    val data = sc.textFile(inputpath)
    val rawgraph = GraphUtils.graphLoader[VertexValue, String](data)
    LOG.info("Lines of input: " + data.count())


    val ccgraph = rawgraph.outerJoinVertices(GraphUtils.largestConnectedComponent(rawgraph))(
      (id , vval, cc) => VertexValue(vval.name, cc.getOrElse(-1d), vval.degree)
    )
      /**
        * Filter out vertices that aren't in the largest connected component
        * */
      .subgraph(
      vpred = (id, vertval) => vertval.vval != -1d
    )

    val cleanedGraph = ccgraph
      .mapVertices((id, v) => VertexValue(v.name, 0, 1))
      .outerJoinVertices(GraphUtils.removeStopVertices(ccgraph, vertexFilterTolerance = 10045000))( // !!!
      /**
        *  Any vertex with a value of -1 can be assumed to be a stop vertex.  This is as all other vertices were labelled as 1 before this step.
        * */
      (id, vertval, cleanedval) => VertexValue(vertval.name, cleanedval.getOrElse(-1d), -1))
       .subgraph(
         vpred = (id, v) => v.vval != -1d
       )

    // recompute degree
   val graph = ccgraph
      .outerJoinVertices(ccgraph.degrees)((id, vval, deg) => VertexValue(vval.name, 0, deg.get))
      .cache()


    /**
      *  Many are called phase
      *  This phase aims to cast a broad net and include vertices with any relevance.
      *  This is done using the spreading activation algorithm
      * */

    val spreadVerts = GraphSearch.spreadActivationWithKeywords(graph.mapVertices((id, vertex) => vertex.name), keywords, maxWaves = 1, maxSteps = 3, firetol = 0.1)


    /**
      * Many are called phase
      * */

    val spreadActivationGraph = graph.outerJoinVertices(spreadVerts)(
      (id, vertex, spreadVertex) => VertexValue(vertex.name, spreadVertex.getOrElse(-1d).asInstanceOf[Double], vertex.degree)
    )
      .subgraph(
        vpred = (id, vertval) => vertval.vval != -1
      )

    //LOG.info("Spreading activation graph: " + spreadActivationGraph.vertices.count())

    /**
      * Few are chosen phase
      * */

    val closeness =
      spreadActivationGraph.outerJoinVertices(
        GraphCentrality.focusedCloseness(spreadActivationGraph.mapVertices((id, v) => v.name), keywords)
      )((id, v, c) => VertexValue(v.name, c.get, v.degree))

    /**
      * Final results
      * Filter out all source vertices
      *
      * */

    val finalFilteredVerts = closeness
      .vertices
      .map(vertval => (vertval._2.name, vertval._2.vval))
      .filter(vert => {
        var isSource = false
        for (k <- keywords) {
          if (vert._1.toLowerCase.contains(k.toLowerCase)) {
            isSource = true
          }
        }
        !isSource
      })

      finalFilteredVerts
      .sortBy(_._2, ascending = false)
      .take(100).foreach(println)

    /**
      * End of computation
      */

    if (!"-".equals(outputpath)) {

      finalFilteredVerts.saveAsTextFile(outputpath)
      LOG.info("Successfully persisted")
    }

    System.exit(0)
  }

  def getGraph(data: RDD[String], sampleRation: Double): Graph[VertexValue, String] = {

    //.sample(false, sampleRatio, (Math.random()*data.count()).asInstanceOf[Long])

    val verts: RDD[(VertexId, VertexValue)] = data
      // NB remove literals here
      //.filter(!_.contains("regexsrtsrtart"))
      .map(
      line => {
        val vtext = line.trim().split(" ")(0)
        (MurmurHash.stringHash(vtext).asInstanceOf[VertexId], VertexValue(vtext, 0, 0))
      })
      .union(data.map(
        line => {
          val vtext = line.trim().split(" ")(2)
          (MurmurHash.stringHash(vtext).asInstanceOf[VertexId], VertexValue(vtext, 0, 0))
        })
      )

    println("lines: " + data.count())

    val edges: RDD[Edge[String]] = data.map(
      line => {
        val tokes = line.trim().split(" ")
        val srcId = MurmurHash.stringHash(tokes(0))
        val edgeAttr = tokes(1)
        val dstId = MurmurHash.stringHash(tokes(2))

        Edge(srcId, dstId, edgeAttr)
      }
    )

    val g = Graph(verts, edges)

    val degg = g
      .outerJoinVertices(g.degrees)((id, vval, deg) => VertexValue(vval.name, 0, deg.get))

    /*graph
      .outerJoinVertices(graphnodeg.inDegrees) ((id, vval, indeg) => VertexValue(vval.vname, indeg.getOrElse(-1).asInstanceOf[Double], vval.degree))
      .outerJoinVertices(graphnodeg.outDegrees) ((id, vval, outdeg) => VertexValue(vval.vname, outdeg.getOrElse(0).asInstanceOf[Double]/vval.vval, vval.degree))
      .outerJoinVertices(graphnodeg.degrees) ((id, vval, deg) => VertexValue(vval.vname, vval.vval*deg.get , vval.degree))
      .vertices
      .map(v => (v._2.vname, v._2.vval))
      .sortBy(_._2, ascending = false)
      .take(100)*/




    //val retg = degg.outerJoinVertices(
    //  sc.parallelize(degg.degrees.sortBy(_._2, ascending = false).take(100))
    //)((id, v, x) => VertexValue(v.vname, x.getOrElse(-1).asInstanceOf[Double], v.degree))
    //  .subgraph(
    //    vpred = (id, v) => v.vval == -1
    //  )
    val retg = degg
    println(retg.vertices.count())

    /**
      * Vertices with a very high in / out degree ratio are likely to be abstract concepts and should not be included in the graph
      *
      * */
    /*val retg = degg
      .outerJoinVertices(degg.inDegrees)((id, v, d) => VertexValue(v.vname, d.getOrElse(1).asInstanceOf[Double], v.degree))
      .outerJoinVertices(degg.outDegrees)((id, v, d) => VertexValue(v.vname, v.vval/(d.getOrElse(1).asInstanceOf[Double]), v.degree))
      .subgraph(
        vpred = (id, v) => v.vval < 100
      )*/

    retg
      .outerJoinVertices(retg.degrees)((id, v, deg) => VertexValue(v.name, v.vval, deg.getOrElse(0)))
  }

  def getLargestConnectedComponent(graph: Graph[VertexValue, String]): Graph[VertexValue, String] = {

    if (graph.vertices.count() < 1) {
      println("[ERROR] format: inputpath outputpath  keywords .....")
      return null
    }

    // get connected component

    val ccmaxid = graph
      .connectedComponents()
      .vertices
      .map(v => (v._2, 1))
      .reduceByKey(_ + _)
      .sortBy(_._2, ascending = false)
      .take(1)(0)._1

    val g = graph
      .outerJoinVertices(graph.connectedComponents().vertices)((id, vval, ccid) => VertexValue(vval.name, ccid.get, vval.degree))
      .subgraph(vpred = (vid, vval) => vval.vval == ccmaxid)
      .mapVertices((id, v) => VertexValue(v.name, 0, v.degree))

    println("getlargestcc: size: " + g.vertices.count())
    g.outerJoinVertices(g.degrees)((id, v, d) => VertexValue(v.name, v.vval, d.getOrElse(-1)))

  }

  def ssp(g: Graph[VertexValue, String], source: VertexId, maxsteps: Int = 4): Graph[VertexValue, String] = {
    println("ssp")

    val bignum = 100000

    val ssp = g
      .mapVertices((vid, vval) => {
        var v = 100000
        if (vid == source) {
          v = 0
        }
        VertexValue(vval.name, v, vval.degree)
      })
      .pregel(bignum, maxsteps, EdgeDirection.Either)(
        vprog = (id, vval, msgs) => VertexValue(vval.name, vval.vval min msgs, vval.degree),
        mergeMsg = (m1, m2) => m1 min m2,
        sendMsg = triplet => {
          val edgeweight = 1
          if (triplet.dstAttr.vval > triplet.srcAttr.vval + edgeweight) {
            //println("src->dst ")
            Iterator((triplet.dstId, triplet.srcAttr.vval.asInstanceOf[Int] + edgeweight))
          } else if (triplet.srcAttr.vval > triplet.dstAttr.vval + edgeweight) {
            //println("dst->src")
            Iterator((triplet.srcId, triplet.dstAttr.vval.asInstanceOf[Int] + edgeweight))
          }
          else
            Iterator.empty
        }
      )

    val totaldist = ssp.vertices
      .map {
        vert => vert._2.vval
      }
      .reduce(_ + _)

    println("Total distance count: " + source + ": " + totaldist)

    ssp
  }

  /**
    * Takes a list of keywords and checks if vertices contains these keywords.  A breadth first search is then run using these vertices as the sources.
    * */
  def breadthFirstSearch (g: Graph[VertexValue, String],
                          keywords: List[String],
                          firetol: Double = 1.1d,
                          maxsteps: Int = 5): Graph[VertexValue, String] = {

    val initialmsg = 0d

    val ret = g
      .mapVertices((id, v) => {
        // initialise source vertices to 1
        var hits = 0

        for (k <- keywords) {
          if (v.name.toLowerCase().contains(k.toLowerCase())) {
            //VertexValue(v.vname, 1, v.degree)
            hits += 1
          }
        }
        VertexValue(v.name, if (hits > 0) 1 else 0, v.degree)

      })
      .pregel(initialmsg, maxsteps, EdgeDirection.Either)(
        // Pregel implementation of Breadth first search
        vprog = (id, v, s) => VertexValue(v.name, v.vval + s, v.degree),
        sendMsg = trip => {
          if (trip.srcAttr.vval >= 1) {
            Iterator((trip.dstId, trip.srcAttr.vval))
          } else if (trip.dstAttr.vval > 0) {
            Iterator((trip.srcId, trip.dstAttr.vval))
          } else {
            Iterator.empty
          }
        },
        mergeMsg = (m1, m2) => m1 + m2
      )
      .subgraph(
        vpred = (id, vval) => vval.vval >= 1
      )

    println("activation num: " + ret.vertices.filter(_._2.vval > 1.1).count())

    ret.outerJoinVertices(ret.degrees)((id, v, d) => VertexValue(v.name, v.vval, d.getOrElse(-1)))
  }


  /**
    *  Takes a list of keywords and uses vertices containing these words as source vertices.  The spreading activation algorithm is then run and the graph returned
    * */
  def spreadActivationWithKeywords(g: Graph[VertexValue, String],
                                   keywords: List[String],
                                   firetol: Double = 0.25d,
                                   maxsteps: Int = 1,
                                   degreePenalty: Float = 1.25f,
                                   maxIters: Long = 10,
                                   activeVertsNeeded: Long = 10000): Graph[VertexValue, String] = {

    println("Spreading activation w keywords")

    /**
      * Initial activation of user profile vertices: firetol
      * Iteration: wave, restart: phase
      *
      * Whenever the number of activated nodes stays the same between wave w 1 and wave w, then no further activa- tion is possible without firing all activated nodes again
      *
      * */
    val initialmsg = 0d
    var sagraph = g
    var activatedCount: Long = 0l
    var wave: Int = 0
    //var cccount = 0d

    do {
      var initialActive = 0

      sagraph = sagraph
        .mapVertices((id, v) => {
            // vertex was active in previous round?
            if (v.activated || v.sourceVert) {
              initialActive = initialActive + 1
              VertexValue(v.name, if (v.vval > firetol) v.vval else firetol, v.degree, shouldFire = true, activated = true, sourceVert = v.sourceVert)
            }else {
              var active = false
              var sourceVert = false
              // check if vertex in keyworks
              for (k <- keywords.map(_.toLowerCase)) {
                if (v.name.toLowerCase().contains(k)) {
                  active = true
                  sourceVert = true
                  initialActive = initialActive + 1
                }
              }
              VertexValue(v.name, if (sourceVert) firetol else 0d /*v.vval*/, v.degree, shouldFire = active, activated = active, sourceVert = sourceVert)
            }
          })
        /*.mapVertices((id, v) => {
          /**
            * Initialise all source vertices to the fire tolerance
            * */
          var activated = false

          if (v.activated /*v.vval >= firetol */) {
            initialActive = initialActive + 1
            activated = true
            VertexValue(v.vname, v.vval, v.degree, shouldFire = true, activated=true)
          } else {

            /**
              * If vertname contains keyword, set it to be active
              * */
            for (k <- keywords) {
              if (v.vname.toLowerCase().contains(k.toLowerCase())) {
                activated = true
              }
            }

            if (activated) initialActive = initialActive + 1

            // calculate newvval
            var newvval = v.vval
            if (v.vval < firetol && activated) {
              newvval = firetol
            }

            VertexValue(v.vname, if (activated)  newvval else 0, v.degree, shouldFire = activated, activated=activated)
          }
        })*/
        .pregel(initialmsg, 5, EdgeDirection.Either)(
          vprog = (id, v, msg) => {
            val newvval = v.vval + msg

            if (newvval < firetol) {
              VertexValue(v.name, newvval, v.degree, shouldFire = v.shouldFire, activated = false, sourceVert = v.sourceVert)
            }
              // if the new value is over the tolerance, and the previous wasnt, we should fire
            else if (newvval > firetol && v.vval < firetol) {
              VertexValue(v.name, newvval, v.degree, shouldFire = true, activated = true, sourceVert = v.sourceVert)
            }
            else if (v.vval > firetol) { // vertex has already fired and shouldn't again unless new wave !!
              VertexValue(v.name, newvval, v.degree, shouldFire = false, activated = true, sourceVert = v.sourceVert)
            }
            else  { // default
              VertexValue(v.name, newvval, v.degree, shouldFire = v.shouldFire, activated = v.activated, sourceVert = v.sourceVert)
            }
          },
          mergeMsg = (m1, m2) => m1 + m2,
          sendMsg = (triplet) => {
            if (triplet.srcAttr.shouldFire) {
              Iterator((triplet.dstId, triplet.srcAttr.vval/triplet.srcAttr.degree))
            }
            if (triplet.dstAttr.shouldFire) { // because direction is disregarded
              Iterator((triplet.srcId, triplet.dstAttr.vval/triplet.srcAttr.degree))
            }
            else {
              Iterator.empty
            }
          }
        )

      var almostactivatedCount = sagraph.vertices.filter(_._2.vval > 0).count()
      activatedCount = sagraph.vertices.filter(_._2.vval >= firetol).count()
      // check largest cc ?? val or var

        //cccount = getLargestConnectedComponent(
        //  sagraph.subgraph(
        //    vpred = (id, v) => v.vval > 0
        //  )
        //).vertices.count()
      val totalactivation = sagraph.vertices.map(v => v._2.vval).reduce(_ + _)

      println("wave: " + wave + "  active count: " + activatedCount, "touched: " + almostactivatedCount, sagraph.vertices.filter(_._2.vval > 0).count(), "initial active: " + initialActive, "total activation = " + totalactivation)
      wave = wave + 1
    } while (activatedCount < activeVertsNeeded && wave < maxIters)

    return sagraph
      .subgraph(
        vpred = (id, v) => v.vval > 0
      )
  }

  /*

  def spreadActivation(g: Graph[VertexValue, String],
                       sources: RDD[VertexId],
                       firetol: Double = 1.1d,
                       maxsteps: Int = 1,
                       activeVertsNeeded: Long = 1000): Graph[VertexValue, String] = {


    val initialmsg = 0d
    var sagraph = g
    var activatedCount: Long = 0l
    var wave: Int = 0
    val maxIters = 4

    do {
      var initialActive = 0

      sagraph = sagraph
        // activate sources
        .outerJoinVertices(
        sources.map(v => (v, firetol)))((id, v, s) => VertexValue(v.vname, s.getOrElse(0), v.degree, shouldFire = s.getOrElse(-1) != -1))
        .mapVertices((id, v) => {
          // vertex was active in previous round?
          if (v.activated) {
            initialActive = initialActive + 1
            VertexValue(v.vname, v.vval, v.degree, shouldFire = true, activated = true)
          }else {
            VertexValue(v.vname,  v.vval, v.degree, shouldFire = false, activated = false)
          }
        })
        .pregel(initialmsg, 1, EdgeDirection.Either)(
        vprog = (id, v, msg) => {
          val newvval = v.vval + msg

          if (newvval < firetol) {
            VertexValue(v.vname, newvval, v.degree, shouldFire = v.shouldFire, activated = false)
          }
          // if the new value is over the tolerance, and the previous wasnt, we should fire
          else if (newvval > firetol && v.vval < firetol) {
            VertexValue(v.vname, newvval, v.degree, shouldFire = true, activated = true)
          }
          else if (v.vval > firetol) { // vertex has already fired and shouldn't again unless new wave !!
            VertexValue(v.vname, newvval, v.degree, shouldFire = false, activated = true)
          }
          else  { // default
            VertexValue(v.vname, newvval, v.degree, shouldFire = v.shouldFire, activated = v.activated)
          }
        },
        mergeMsg = (m1, m2) => m1 + m2,
        sendMsg = (triplet) => {
          if (triplet.srcAttr.shouldFire) {
            Iterator((triplet.dstId, triplet.srcAttr.vval/triplet.srcAttr.degree))
          }
          if (triplet.dstAttr.shouldFire) { // because direction is disregarded
            Iterator((triplet.srcId, triplet.dstAttr.vval/triplet.srcAttr.degree))
          }
          else {
            Iterator.empty
          }
        }
      )
      var almostactivatedCount = sagraph.vertices.filter(_._2.vval > 0).count()
      activatedCount = sagraph.vertices.filter(_._2.vval > firetol).count()
      // check largest cc ?? val or var

      //cccount = getLargestConnectedComponent(
      //  sagraph.subgraph(
      //    vpred = (id, v) => v.vval > 0
      //  )
      //).vertices.count()


      val totalactivation = sagraph.vertices.map(v => v._2.vval).reduce(_ + _)

      println("wave: " + wave + "  active count: " + activatedCount, "touched: " + almostactivatedCount, sagraph.vertices.filter(_._2.vval > 0).count(), "initial active: " + initialActive, "total activation = " + totalactivation)
      wave = wave + 1
    } while (activatedCount < activeVertsNeeded && wave < maxIters)

    return sagraph
      .subgraph(
        vpred = (id, v) => v.vval > 0
      )




}

def closeness[VD: ClassTag, ED: ClassTag](graph: Graph[VertexValue, ED], sources: List[VertexId]): Graph[Double, ED] = {

    Graph(
      ShortestPaths.run(
        //graph, graph.vertices.filter(v => sources.contains(v._1))
        /**
          * Get shortest path, using source vertices as landmarks
          * */

        graph, graph.vertices.filter(_._2.sourceVert)
          .map(
            // map into list of IDs
            vert => vert._1
          ).collect())
        .vertices.map {
        vert => (vert._1, {
          val dx = 1.0 / vert._2.values.seq.avg
          if (dx.isNaN | dx.isNegInfinity | dx.isPosInfinity) 0.0 else dx
        })
    }: RDD[(VertexId, Double)], graph.edges)

  }

  def average[T](ts: Iterable[T])(implicit num: Numeric[T]) = {
    num.toDouble(ts.sum) / ts.size
  }
  implicit def iterableWithAvg[T: Numeric](data: Iterable[T]): Object {def avg: Double} = new {
    def avg = average(data)
  }

  def writeTriples(triplets: RDD[EdgeTriplet[VertexValue, String]], path: String) = {
    triplets
      .map(t => t.srcAttr.vname + " " + t.attr + " " + t.dstAttr.vname)
      .saveAsTextFile(path)
  }
*/
}