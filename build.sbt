import AssemblyKeys._

assemblySettings

fork := true

jarName  := "SparkTestFatJar.jar"

name := "SparkTest"

version := "1.0"

//scalaVersion := "2.11.7"
scalaVersion := "2.10.6"
//scalaVersion := "2.9.2"
publishMavenStyle := true

//libraryDependencies += "org.apache.spark" %% "spark-core" % "1.5.2"
//libraryDependencies += "org.apache.spark" %% "spark-graphx" % "1.5.2"

libraryDependencies += "org.apache.spark" % "spark-core_2.10" % "1.1.1"
libraryDependencies += "org.apache.spark" % "spark-graphx_2.10" % "1.1.1"

mainClass in Compile := Some("joshbaryia.Driver")
